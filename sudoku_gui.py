
from tkinter import *
from tkinter import filedialog
from solve_sudoku import *



sudoku_input = []
sudoku_labels = []
solution = []
chunks = 0
entries = []
is_default_board = True

def open_file_input(path):
    try:
        with open(path, "r") as f:
            sudoku_in = []
            for line in f.readlines():
                #converting string line from file to integer list
                int_line = list(map(lambda t: int(t), line.replace('\n', '').split()))
                sudoku_in.append(int_line)
            global sudoku_input
            global chunks
            sudoku_input = sudoku_input + sudoku_in
            chunks = len(sudoku_input[0])
            return sudoku_in
    except Exception as e:
        sys.exit(e)

def solve_sudoku():

    output_file = "output_sudoku.cnf"
    global sudoku_input
    global chunks
    global solution
    global is_default_board
    chunks = len(sudoku_input[0])
    if is_default_board:
        new_input = sudoku_input
    else:
        new_input = split_to_chunks( [int(e.get()) for e in entries],chunks)
    dimacs = make_cnf_dimacs(new_input,output_file)
    sat_output = "sat_sudoku_solution.txt"
    os.system(f"minisat {output_file} {sat_output}")
    solution = split_to_chunks(decode_output(sat_output, dimacs),chunks)
    write_solution()




def split_to_chunks(lst,n):
    return [lst[i:i + n] for i in range(0, len(lst), n)]


# Function for opening the
# file explorer window
def browseFiles():

    filename  = filedialog.askopenfilename(initialdir='/',title="Select a File",filetypes=(("Text files","*.txt*"),("all files","*.*")))
    label_file_explorer.configure(text="File Opened: "+filename)
    sudoku_inputs = open_file_input(filename)
    return sudoku_inputs





'''
def printResult(line, window):
        for l in line:
                Label(window, text=l).grid(column=1,row=(3+(line.index(l))))
'''

def GridColor(i,j):
    z=(3*int((i-1)/3))+1 + int((j-1)/3)
    z= "light blue"
    return z



def create_custom_board():
    global entries
    global chunks
    global sudoku_input
    global is_default_board
    if is_default_board:
        is_default_board = False
    print('chunks:\n',chunks)
    t = int(chunks)
    s = t+1
    i,j=40,40
    p=40
    q=260
    for m in range(s):
        if(m%3==0):
            canvas.create_line(i,j,p,q,width=2.5)
        else:
            canvas.create_line(i,j,p,q,width=2)
        i+=30
        p+=30

    c,d=310,40
    for m in range(s):
        canvas.create_line(i,j,c,d,width=2.3)
        j+=24.5
        d+=24.5
    a,b=41.4,121.4
    for i in range(t):
        for j in range(t):
            E = Entry(window, width=2, font="bold", relief="sunken")
            E.grid(row=i+1,column=j+1,  padx=3, pady=2)
            E.configure(bg=GridColor(i+1,j+1))
            E.place(x=a, y=b, height=20, width=25)
            E.insert(0,str(sudoku_input[i][j]))
            entries.append(E)
            a+=30.0
        b+=24.5
        a=41.2
    maze=[[0 for x in range(t)]for y in range(t)]

																								

# Create the root window
window = Tk()
window.title('Sudoku 25x25 solver')
window.geometry("1366x768")

window.config(background = "white")


def prettyfy(matrix):
    s = [[str(e) for e in row] for row in matrix]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    return table




def create_board():
    global window
    global sudoku_input
    global sudoku_labels
    global is_default_board
    if not is_default_board:
        is_default_board = True
    new_table = prettyfy(sudoku_input)
    for i in range(len(new_table)):
        row = Label(window, text=new_table[i])
        sudoku_labels.append(row)
    for i,row in enumerate(sudoku_labels):
        row.grid(column=1, row=i + 7)
    is_default_board = True
    return


def write_solution():
    clear_board()
    global window
    global solution
    global entries
    if not is_default_board:

        for i in range(len(solution)):
            for j in range(len(solution[i])):
                entries[chunks*i+j].delete(0,'end')
                entries[chunks*i+j].insert(0,str(solution[i][j]))
    else:
        new_table = prettyfy(solution)
        for i in range(len(new_table)):
            row = Label(window, text=new_table[i])
            sudoku_labels.append(row)
        for i,row in enumerate(sudoku_labels):
            row.grid(column=1, row=i + 7)



def clear_board():
    global window
    global sudoku_input
    global sudoku_labels
    global solution
    global entries
    global is_default_board
    print('win grid slave:\n',window.grid_slaves())
    if is_default_board:
        for label in window.grid_slaves():
            if int(label.grid_info()['row']) >=4:
                label.grid_forget()
        sudoku_labels = []
    else:
        for e in entries:
            e.delete(0,'end')
            e.insert(0,str(0))


canvas = Canvas(window)

label_file_explorer = Label(window,
                            text = "Sudoku Solver",
                            width = 100, height = 4,
                            fg = "blue")
	
button_explore = Button(window,
                        text = "Browse Files",
                        command = browseFiles)
button_clear_board = Button(window,text="Clear Board",command=clear_board)


button_solve = Button(window,text="Solve",command=solve_sudoku)


button_create_board = Button(window,text = "Create Default Board",command = create_board)

button_create_custom_board = Button(window,text = "Create Custom Board",command = create_custom_board)

button_solver = Button(window,
                     text = "Solve Sudoku",
                     )


button_exit = Button(window,
                     text = "Exit",
                     command = exit)




label_file_explorer.grid(column = 1, row = 1)

button_explore.grid(column = 2, row = 2)

button_solve.grid(column=3,row=2)

button_create_board.grid(column=4,row=2)

button_create_custom_board.grid(column=5,row=2)

button_clear_board.grid(column=6,row=2)

button_exit.grid(column = 7,row = 2)


# Label(window,text=line[0]).grid
# Label(window,text=line[1])


window.mainloop()
